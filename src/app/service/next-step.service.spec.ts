import { TestBed } from '@angular/core/testing';

import { NextStepService } from './next-step.service';

describe('NextStepService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NextStepService = TestBed.get(NextStepService);
    expect(service).toBeTruthy();
  });
});
