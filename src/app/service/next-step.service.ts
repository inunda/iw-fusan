import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class NextStepService {
  nextStep$: Observable<any>;
  private myMethodSubject = new Subject<any>();

  constructor() {
    this.nextStep$ = this.myMethodSubject.asObservable();
  }

  myMethod(data) {
    this.myMethodSubject.next(data);
  }
}
