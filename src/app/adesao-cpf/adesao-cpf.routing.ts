import { Routes } from "@angular/router";

import { UsercpfComponent } from "./usercpf/usercpf.component";

export const routes: Routes = [{ path: "", component: UsercpfComponent }];
