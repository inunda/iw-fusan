import { Component, OnInit } from "@angular/core";
import CPF, { validate } from "cpf-check";

@Component({
  selector: "app-usercpf",
  templateUrl: "./usercpf.component.html",
  styleUrls: ["./usercpf.component.css"]
})
export class UsercpfComponent implements OnInit {
  constructor() {}

  usercpf: string = "";

  ngOnInit() {
    const usercpf = this.usercpf;
    if (CPF.validate(usercpf)) {
      return null;
    } else {
      return "valid";
    }
  }
}
