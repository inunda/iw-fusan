import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsercpfComponent } from './usercpf.component';

describe('UsercpfComponent', () => {
  let component: UsercpfComponent;
  let fixture: ComponentFixture<UsercpfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsercpfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsercpfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
