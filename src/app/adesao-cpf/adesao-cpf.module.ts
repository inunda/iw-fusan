import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IMaskModule } from "angular-imask";

import { UsercpfComponent } from "./usercpf/usercpf.component";

import { routes } from "./adesao-cpf.routing";
import { RouterModule } from "@angular/router";

import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    IMaskModule
  ],
  exports: [FormsModule, IMaskModule],
  declarations: [UsercpfComponent]
})
export class AdesaoCpfModule {}
