import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { FormsModule } from "@angular/forms";

import { NgCpfModule } from "ngcpf";

import {
  NgbPaginationModule,
  NgbAlertModule
} from "@ng-bootstrap/ng-bootstrap";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SolicitacaoConcluidaComponent } from "./pages/solicitacao-concluida/solicitacao-concluida.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { HeaderComponent } from "./components/header/header.component";
import { AdesaoConcluidaComponent } from "./pages/adesao-concluida/adesao-concluida.component";

import { HomeModule } from "./home/home.module";
import { AdesaoCpfModule } from "./adesao-cpf/adesao-cpf.module";
import { SolicitacaoAdesaoModule } from "./solicitacao-adesao/solicitacao-adesao.module";
import { AdesaoModule } from "./adesao/adesao.module";

import { NextStepService } from "./service/next-step.service";
import { IMaskModule } from "angular-imask";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    SolicitacaoConcluidaComponent,
    NotFoundComponent,
    HeaderComponent,
    AdesaoConcluidaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    FormsModule,
    NgCpfModule,
    HttpClientModule,
    IMaskModule,
    HomeModule,
    AdesaoCpfModule,
    SolicitacaoAdesaoModule,
    AdesaoModule
  ],
  exports: [
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    FormsModule,
    NgCpfModule,
    HttpClientModule,
    IMaskModule,
    HomeModule,
    AdesaoCpfModule,
    SolicitacaoAdesaoModule,
    AdesaoModule
  ],
  providers: [NextStepService],
  bootstrap: [AppComponent]
})
export class AppModule {}
