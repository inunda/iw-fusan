import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoConcluidaComponent } from './solicitacao-concluida.component';

describe('SolicitacaoConcluidaComponent', () => {
  let component: SolicitacaoConcluidaComponent;
  let fixture: ComponentFixture<SolicitacaoConcluidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoConcluidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoConcluidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
