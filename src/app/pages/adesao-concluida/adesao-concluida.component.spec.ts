import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoConcluidaComponent } from './adesao-concluida.component';

describe('AdesaoConcluidaComponent', () => {
  let component: AdesaoConcluidaComponent;
  let fixture: ComponentFixture<AdesaoConcluidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoConcluidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoConcluidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
