import { Component, OnInit } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-adesao-concluida",
  templateUrl: "./adesao-concluida.component.html",
  styleUrls: ["./adesao-concluida.component.css"]
})
export class AdesaoConcluidaComponent implements OnInit {
  MyData: any;
  public data: any = this.MyData;

  constructor(private NextStepService: NextStepService) {
    this.NextStepService.nextStep$.subscribe(data => {
      this.data = data;
      console.log(this.data);
    });
  }

  ngOnInit() {}
}
