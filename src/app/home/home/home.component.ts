import { Component, OnInit } from "@angular/core";
import CPF, { validate } from "cpf-check";
import { IMaskModule } from "angular-imask";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  usercpf: number = null;
  userpassw: string = "";
  btnlogin: boolean = false;

  constructor() {}

  validLoginCpf(usercpf: number) {
    if (CPF.validate(usercpf)) {
      this.usercpf = usercpf;
      if (this.userpassw !== "" && this.usercpf !== null) {
        return this.btnlogin = true
      } else {
        return this.btnlogin = false
      }
    } else {
      return (this.btnlogin = false);
    }
  }

  validLoginPassw(userpassw: string) {
    if (userpassw !== "") {
      this.userpassw = userpassw;
      if (this.userpassw !== "" && this.usercpf !== null) {
        return this.btnlogin = true
      } else {
        this.userpassw = ""
        return this.btnlogin = false
      }
    } else {
      this.userpassw = ""
      return (this.btnlogin = false);
    }
  }

  ngOnInit() {}
}
