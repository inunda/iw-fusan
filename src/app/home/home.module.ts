import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IMaskModule } from "angular-imask";

import { HomeComponent } from "./home/home.component";

import { routes } from "./home.routing";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [RouterModule.forChild(routes), IMaskModule, CommonModule],
  exports: [IMaskModule],
  declarations: [HomeComponent]
})
export class HomeModule {}
