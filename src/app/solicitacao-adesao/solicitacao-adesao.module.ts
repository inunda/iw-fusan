import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { IMaskModule } from "angular-imask";

import { routes } from "./solicitacao-adesao.routing";

import { SolicitacaoAdesaoAcessoComponent } from "./solicitacao-adesao-acesso/solicitacao-adesao-acesso.component";
import { SolicitacaoAdesaoDadosPessoaisComponent } from "./solicitacao-adesao-dados-pessoais/solicitacao-adesao-dados-pessoais.component";
import { SolicitacaoAdesaoComponent } from "./solicitacao-adesao/solicitacao-adesao.component";

@NgModule({
  declarations: [
    SolicitacaoAdesaoComponent,
    SolicitacaoAdesaoAcessoComponent,
    SolicitacaoAdesaoDadosPessoaisComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    IMaskModule
  ],
  exports: [FormsModule, IMaskModule]
})
export class SolicitacaoAdesaoModule {}
