import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoAdesaoComponent } from './solicitacao-adesao.component';

describe('SolicitacaoAdesaoComponent', () => {
  let component: SolicitacaoAdesaoComponent;
  let fixture: ComponentFixture<SolicitacaoAdesaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoAdesaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoAdesaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
