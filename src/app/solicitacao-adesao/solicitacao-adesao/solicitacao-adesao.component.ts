import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import Stepper from "bs-stepper";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-solicitacao-adesao",
  templateUrl: "./solicitacao-adesao.component.html",
  styleUrls: ["./solicitacao-adesao.component.css"],
  providers: []
})
export class SolicitacaoAdesaoComponent implements OnInit {
  BtnDisabledOne: boolean = true;
  BtnDisabledTwo: boolean = true;

  MyData: any;
  public data: any = this.MyData;

  public constructor(private myService: NextStepService) {
    this.myService.nextStep$.subscribe(data => {
      this.data = data;
      switch (data.tab) {
        case "zero":
          if (
            data.complete === true ||
            (data.usernome !== "" &&
              data.useremail !== "" &&
              data.userphone !== "" &&
              data.userphone.length === 11)
          ) {
            return (this.BtnDisabledOne = false);
          } else {
            return (this.BtnDisabledOne = true);
          }
        case "one":
          if (data.passwordone === data.passwordtwo) {
            return (this.BtnDisabledTwo = false);
          } else {
            return (this.BtnDisabledTwo = true);
          }
      }
    });
  }

  private stepper: Stepper;

  next() {
    this.stepper.next();
  }

  onSubmit() {}

  ngOnInit() {
    this.stepper = new Stepper(document.querySelector("#stepper1"), {
      linear: true,
      animation: true
    });
  }
}
