import { Component, OnInit } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-solicitacao-adesao-acesso",
  templateUrl: "./solicitacao-adesao-acesso.component.html",
  styleUrls: ["./solicitacao-adesao-acesso.component.css"]
})
export class SolicitacaoAdesaoAcessoComponent implements OnInit {
  passwone: string = "";
  passwtwo: string = "";

  userinfo: any = {
    tab: "one",
    passwordone: "",
    passwordtwo: ""
  };

  constructor(private NextStepService: NextStepService) {}
  public data: any = this.userinfo;

  onUserPasswordOneChange(passwordone: string): void {
    if (passwordone.length >= 8 && passwordone !== "") {
      this.userinfo.passwordone = passwordone;
      this.passwone = passwordone;
      this.NextStepService.myMethod(this.data);
    }
  }

  onUserPasswordTwoChange(passwordtwo: string): void {
    if (passwordtwo.length >= 8 && passwordtwo !== "") {
      this.userinfo.passwordtwo = passwordtwo;
      this.passwtwo = passwordtwo;
      this.NextStepService.myMethod(this.data);
    }
  }

  ngOnInit() {}
}
