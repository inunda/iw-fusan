import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoAdesaoAcessoComponent } from './solicitacao-adesao-acesso.component';

describe('SolicitacaoAdesaoAcessoComponent', () => {
  let component: SolicitacaoAdesaoAcessoComponent;
  let fixture: ComponentFixture<SolicitacaoAdesaoAcessoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoAdesaoAcessoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoAdesaoAcessoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
