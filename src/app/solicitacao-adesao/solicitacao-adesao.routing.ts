import { Routes } from "@angular/router";

import { SolicitacaoAdesaoComponent } from "./solicitacao-adesao/solicitacao-adesao.component";

export const routes: Routes = [
  { path: "", component: SolicitacaoAdesaoComponent }
];
