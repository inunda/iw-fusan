import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitacaoAdesaoDadosPessoaisComponent } from './solicitacao-adesao-dados-pessoais.component';

describe('SolicitacaoAdesaoDadosPessoaisComponent', () => {
  let component: SolicitacaoAdesaoDadosPessoaisComponent;
  let fixture: ComponentFixture<SolicitacaoAdesaoDadosPessoaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitacaoAdesaoDadosPessoaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitacaoAdesaoDadosPessoaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
