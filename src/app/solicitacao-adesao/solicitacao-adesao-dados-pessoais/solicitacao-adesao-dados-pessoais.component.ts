import { Component, OnInit, OnChanges } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { IMaskModule } from "angular-imask";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-solicitacao-adesao-dados-pessoais",
  templateUrl: "./solicitacao-adesao-dados-pessoais.component.html",
  styleUrls: ["./solicitacao-adesao-dados-pessoais.component.css"]
})
export class SolicitacaoAdesaoDadosPessoaisComponent implements OnInit {
  usernome: string;
  useremail: string;
  userphone: string;

  phonetype: number = null;

  nomeError: boolean = false;
  emailError: boolean = false;
  phoneError: boolean = false;

  userinfo: any = {
    tab: "zero",
    complete: false,
    usernome: "",
    useremail: "",
    userphone: ""
  };

  realUser: any = {
    nome: "Nazgul",
    email: "nazgul@sauron.com",
    cpf: "67675467710",
    userphone: "419955646482"
  };

  public constructor(
    private NextStepService: NextStepService,
    private route: ActivatedRoute
  ) {}

  public data: any = this.userinfo;

  onUserNomeChange(nome: string): void {
    let re = /^[A-Za-z ]*$/gm;
    if (re.test(String(nome).toLowerCase())) {
      this.userinfo.usernome = nome;
      this.usernome = nome;
      this.NextStepService.myMethod(this.data);
      this.nomeError = false;
    } else {
      this.nomeError = true;
    }
  }

  onUserEmailChange(email: string): void {
    let re = /\S+@\S+\.\S+/;
    if (re.test(String(email).toLowerCase())) {
      this.userinfo.useremail = email;
      this.useremail = email;
      this.NextStepService.myMethod(this.data);
      this.emailError = false;
    } else {
      this.emailError = true;
    }
  }

  onUserPhoneChange(phone: string): void {
    console.log(phone.length);
    this.phonetype = phone.replace(/[^A-Z0-9]+/gi, "").length;
    this.userinfo.userphone = phone.replace(/[^A-Z0-9]+/gi, "");
    this.userphone = phone.replace(/[^A-Z0-9]+/gi, "");
    this.NextStepService.myMethod(this.data);
  }

  ngOnInit() {
    let cpfnumber: string;
    let cpfdatabase: string = this.realUser.cpf;
    this.route.queryParams.subscribe(params => {
      cpfnumber = String(params.params);
    });
    if (cpfnumber === cpfdatabase) {
      this.userinfo.complete = true;
      this.usernome = this.realUser.nome;
      this.useremail = this.realUser.email;
      this.userphone = this.realUser.userphone;
      this.NextStepService.myMethod(this.data);
    } else {
      this.usernome = "";
      this.useremail = "";
      this.userphone = "";
    }
  }
}
