import { Component, OnInit } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-adesao-assinatura-eletronica",
  templateUrl: "./adesao-assinatura-eletronica.component.html",
  styleUrls: ["./adesao-assinatura-eletronica.component.css"]
})
export class AdesaoAssinaturaEletronicaComponent implements OnInit {
  assinatura: string = "";

  userbirth: any = {
    tab: "seven",
    assinatura: ""
  };

  public constructor(private NextStepService: NextStepService) {}

  public datas: any = this.userbirth;

  onChangeAssinatura(assinatura: string): void {
    if (assinatura !== "") {
      this.userbirth.assinatura = assinatura;
      this.assinatura = assinatura;
      this.NextStepService.myMethod(this.datas);
    }
  }

  ngOnInit() {}
}
