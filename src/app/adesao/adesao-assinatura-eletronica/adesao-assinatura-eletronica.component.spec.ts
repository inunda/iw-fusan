import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoAssinaturaEletronicaComponent } from './adesao-assinatura-eletronica.component';

describe('AdesaoAssinaturaEletronicaComponent', () => {
  let component: AdesaoAssinaturaEletronicaComponent;
  let fixture: ComponentFixture<AdesaoAssinaturaEletronicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoAssinaturaEletronicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoAssinaturaEletronicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
