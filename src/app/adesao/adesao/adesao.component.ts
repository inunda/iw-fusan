import {
  Component,
  ViewEncapsulation,
  OnInit,
  OnChanges,
  ViewChild,
  ElementRef
} from "@angular/core";
import Stepper from "bs-stepper";

import { NextStepService } from "../../service/next-step.service";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: "app-adesao",
  templateUrl: "./adesao.component.html",
  styleUrls: ["./adesao.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class AdesaoComponent implements OnInit, OnChanges {
  @ViewChild("tref", { read: ElementRef, static: true }) tref: ElementRef;
  @ViewChild("trefDois", { read: ElementRef, static: true })
  trefDois: ElementRef;
  @ViewChild("box", { read: ElementRef, static: true }) box: ElementRef;
  @ViewChild("header", { read: ElementRef, static: true }) header: ElementRef;

  BtnDisabledOne: boolean = true;
  BtnDisabledTwo: boolean = true;
  BtnDisabledThree: boolean = true;
  BtnDisabledFour: boolean = true;
  BtnDisabledFive: boolean = true;
  BtnDisabledSix: boolean = true;
  BtnDisabledSeven: boolean = true;
  BtnDisabledEight: boolean = true;
  BtnCloseAdesao: boolean = false;
  BtnNexModalAdesao: boolean = false;

  enabledTabTwo: boolean = false;
  enabledTabThree: boolean = false;
  enabledTabFour: boolean = false;
  enabledTabFive: boolean = false;
  enabledTabSix: boolean = false;
  enabledTabSeven: boolean = false;
  enabledTabEight: boolean = false;

  moreThan100: boolean = false;

  prevPos: any = 0;

  nome: string = "";
  cpf: number = null;

  MyData: any;
  public data: any = this.MyData;

  // Habilita e desabilita - VINCULAR FAMILIARES
  vincularFamiliar: boolean = true;

  menorDeIdade: boolean = false;
  maioriaddeInfo: boolean = false;
  maioridadenext: boolean = false;
  maiorback: boolean = true;

  testeprops: any = {
    tributacao: "",
    valormensal: "",
    vencimento: "",
    qntbeneficiarios: "",
    beneficiarios: ""
  };

  public constructor(
    private NextStepService: NextStepService,
    private modalService: NgbModal
  ) {
    this.NextStepService.nextStep$.subscribe(data => {
      this.data = data;

      if (this.data !== null || this.data !== undefined) {
        switch (this.data.tab) {
          case "one":
            // DADOS PESSOAIS
            if (new Date().getFullYear() - this.data.birth.slice(0, 4) > 18) {
              this.maioriaddeInfo = false;

              if (
                this.data.birth !== "" &&
                this.data.rgval !== "" &&
                this.data.userx !== "" &&
                this.data.users !== "" &&
                this.data.usern !== ""
              ) {
                return (
                  (this.BtnDisabledOne = false), (this.enabledTabTwo = true)
                );
              } else {
                return (
                  (this.BtnDisabledOne = true), (this.enabledTabTwo = false)
                );
              }
            } else {
              this.maioriaddeInfo = true;
              if (
                this.data.birth !== "" &&
                this.data.userx !== "" &&
                this.data.users !== "" &&
                this.data.usern !== ""
              ) {
                return (
                  (this.BtnDisabledOne = false), (this.enabledTabTwo = true)
                );
              } else {
                return (
                  (this.BtnDisabledOne = true), (this.enabledTabTwo = false)
                );
              }
            }

          case "one.one":
            if (
              this.data.usercpf !== "" &&
              this.data.usernome !== "" &&
              this.data.userparentesco !== "" &&
              this.data.userphone !== "" &&
              this.data.useremail !== ""
            ) {
              this.maioridadenext = true;
              this.maiorback = true;
              this.BtnDisabledOne = false;
              this.enabledTabTwo = true;
            } else {
              this.maioridadenext = false;
              this.maiorback = true;
              this.BtnDisabledOne = true;
              this.enabledTabTwo = false;
            }

            break;
          case "two":

            console.log(this.data.endValue)

            // ENDEREÇO
            if (
              this.data.cep !== "" &&
              this.data.endereco !== "" &&
              this.data.bairro !== "" &&
              this.data.estado !== "" &&
              this.data.cidade !== "" &&
              this.data.endValue !== ""
            ) {
              if (!this.vincularFamiliar) {
                this.BtnDisabledTwo = false;
                this.enabledTabFour = true;
                this.enabledTabThree = true;
              } else {
                this.BtnDisabledTwo = true;
                this.enabledTabFour = false;
                this.enabledTabThree = true;
              }
              this.BtnDisabledTwo = false;
              this.enabledTabThree = true;
            } else {
              this.BtnDisabledTwo = true;
              this.enabledTabThree = false;
            }
            break;
          case "three":
            // VINCULAR FAMILIARES
            if (
              this.data.nome !== "" &&
              this.data.sobrenome !== "" &&
              this.data.parentesco !== "" &&
              this.data.cpf !== "" &&
              this.data.check === true
            ) {
              return (
                (this.BtnDisabledThree = false), (this.enabledTabFour = true)
              );
            } else {
              return (
                (this.BtnDisabledThree = true), (this.enabledTabFour = false)
              );
            }
            break;
          case "four":
            //TRIBUTACAO
            if (this.data.one === true || this.data.two === true) {
              if (this.data.one && !this.data.two) {
                this.testeprops.tributacao = "PROGRESSIVO COMPENSÁVEL";
                return (
                  (this.BtnDisabledFour = false), (this.enabledTabFive = true)
                );
              } else {
                this.testeprops.tributacao = "PROGRESSIVO COMPENSÁVEL";
                return (
                  (this.BtnDisabledFour = false), (this.enabledTabFive = true)
                );
              }
            } else {
              return (
                (this.BtnDisabledFour = true), (this.enabledTabFive = false)
              );
            }
            break;

          case "five":
            // CONTRIBUICAO

            if (this.data.truevalue == false && this.data.valuetwo >= 0) {
              this.testeprops.valormensal = `R$ ${this.data.valueone},00`;
              this.BtnDisabledFive = false;
              this.enabledTabSix = true;
            } else {
              this.BtnDisabledFive = true;
              this.enabledTabSix = false;
            }
            break;

          case "six":
            // TERMOS E ACEITES

            if (
              this.data.optone !== false &&
              this.data.opttwo !== false &&
              (this.data.uspersonyes === "Sim" ||
                this.data.uspersonno === "Nao") &&
              (this.data.politicoyes === "Sim" ||
                this.data.politicono === "Nao") &&
              (this.data.residenciaoutropaisYes === "Sim" ||
                this.data.residenciaoutropaisNo === "Nao")
            ) {
              if (this.data.nif.on === true) {
                if (this.data.nif.arr.length !== 0) {
                  return (
                    (this.BtnDisabledSix = false), (this.enabledTabEight = true)
                  );
                } else {
                  return (
                    (this.BtnDisabledSix = true), (this.enabledTabEight = false)
                  );
                }
              }
              return (
                (this.BtnDisabledSix = false), (this.enabledTabEight = true)
              );
            } else {
              return (
                (this.BtnDisabledSix = true), (this.enabledTabEight = false)
              );
            }
            break;

          case "seven":
            //ASSINATURA
            if (this.data.assinatura.length >= 8) {
              return (this.BtnDisabledSeven = false);
            } else {
              return (this.BtnDisabledSeven = true);
            }
            break;
          case "eight":
            // BENEFICIARIOS
            let num = [];
            if (
              this.data.nome !== "" &&
              this.data.cpf !== "" &&
              this.data.date !== "" &&
              this.data.beneficios !== "" &&
              this.data.parentesco !== null &&
              this.data.qnt !== ""
            ) {
              for (let x = 0; x < this.data.qnt.length; x++) {
                num.push(Number(this.data.qnt[x].beneficios));
              }

              let sumnumber = num.reduce((a, b) => a + b, 0);

              switch (sumnumber) {
                case 100:
                  this.testeprops.qntbeneficiarios = this.data.qnt.length;
                  this.testeprops.beneficiarios = this.data.qnt;
                  return (
                    (this.BtnDisabledEight = false),
                    (this.enabledTabSeven = true),
                    (this.moreThan100 = false)
                  );
                default:
                  return (
                    (this.BtnDisabledEight = true),
                    (this.enabledTabSeven = false),
                    (this.moreThan100 = true)
                  );
              }
            } else {
              return (
                (this.BtnDisabledEight = true),
                (this.enabledTabSeven = false),
                (this.moreThan100 = false)
              );
            }
        }
      }
    });
  }

  private stepper: Stepper;

  next() {
    this.stepper.next();
  }

  cpfDeFuncionario() {}

  backOneTab() {
    this.menorDeIdade = false;
    this.BtnDisabledOne = false;
    this.maioridadenext = false;
  }

  closeAdesao() {
    return (this.BtnCloseAdesao = true);
  }

  onSubmit() {
    return false;
  }

  verificaMaioridade() {
    if (this.maioriaddeInfo) {
      this.menorDeIdade = true;
      this.BtnDisabledOne = true;
      this.maioridadenext = false;
      if (
        (<HTMLInputElement>document.getElementById("cpf-value")).value !== ""
      ) {
        this.maioridadenext = true;
        this.BtnDisabledOne = false;
      }
    } else {
      this.menorDeIdade = false;
      this.menorDeIdade = false;
      this.maioridadenext = false;
    }
  }

  closeResult: string;

  openContrato(contrato) {
    this.modalService
      .open(contrato, {
        windowClass: "modal-contrato",
        ariaLabelledBy: "modal-basic-title"
      })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  onChangePermission(value: any) {
    if (value !== false) {
      return (this.BtnNexModalAdesao = true), (this.enabledTabSeven = true);
    } else {
      return (this.BtnNexModalAdesao = false), (this.enabledTabSeven = false);
    }
  }

  scrollWinOne() {
    this.header.nativeElement.scrollTo(125, 0);
  }

  scrollWinTwo() {
    this.header.nativeElement.scrollTo(260, 0);
  }

  scrollWinThree() {
    this.header.nativeElement.scrollTo(405, 0);
  }

  scrollWinFour() {
    this.header.nativeElement.scrollTo(545, 0);
  }

  scrollWinFive() {
    this.header.nativeElement.scrollTo(680, 0);
  }

  scrollWinSix() {
    this.header.nativeElement.scrollTo(840, 0);
  }

  ngOnInit() {
    this.header.nativeElement.scrollTo(0, 0);

    // outputs `I am span`
    if (!this.vincularFamiliar) {
      this.tref.nativeElement.remove();
      this.trefDois.nativeElement.remove();
      this.box.nativeElement.className = "box adesao hue";
    } else {
      this.box.nativeElement.className = "box adesao";
    }

    this.stepper = new Stepper(document.querySelector("#stepper1"), {
      linear: false,
      animation: true
    });
  }

  ngOnChanges() {}
}
