import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";
import CPF, { validate } from "cpf-check";

@Component({
  selector: "app-adesao-dados-pessoais-titular",
  templateUrl: "./adesao-dados-pessoais-titular.component.html",
  styleUrls: ["./adesao-dados-pessoais-titular.component.css"]
})
export class AdesaoDadosPessoaisTitularComponent implements OnInit {
  @ViewChild("isnotnull", { read: ElementRef, static: true })
  isnotnull: ElementRef;
  usercpf: string = null;
  usernome: string = "";
  userparentesco: string = "";
  userphone: string = "";
  useremail: string = "";

  isNotNull: boolean = false;

  userdata: any = {
    tab: "one.one",
    usercpf: this.usercpf,
    usernome: this.usernome,
    userparentesco: this.userparentesco,
    userphone: this.userphone,
    useremail: this.useremail,
    isnotnull: this.isNotNull
  };

  public data: any = this.userdata;
  public constructor(private NextStepService: NextStepService) {}

  onUserCpfChange(value: string): void {
    if (CPF.validate(value)) {
      this.userdata.usercpf = value.replace(/\./g, "").replace(/\-/g, "");
      this.NextStepService.myMethod(this.data);
    } else {
      this.userdata.usercpf = "";
      this.NextStepService.myMethod(this.data);
    }
  }
  onUserNomeChange(value: string): void {
    this.userdata.usernome = value;
    this.NextStepService.myMethod(this.data);
  }
  onUserParentescoChange(value: string): void {
    this.userdata.userparentesco = value;
    this.NextStepService.myMethod(this.data);
  }
  onUserPhoneChange(value: string): void {
    this.userdata.userphone = value.replace(/[^0-9.]/g, "");
    this.NextStepService.myMethod(this.data);
  }
  onUserEmailChange(value: string): void {
    let re = /\S+@\S+\.\S+/;
    this.userdata.useremail = re.test(value);
    if (this.userdata.useremail) {
      this.NextStepService.myMethod(this.data);
    } else {
      this.userdata.useremail = "";
      this.NextStepService.myMethod(this.data);
    }
  }

  ngOnInit() {}
}
