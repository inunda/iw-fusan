import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoDadosPessoaisTitularComponent } from './adesao-dados-pessoais-titular.component';

describe('AdesaoDadosPessoaisTitularComponent', () => {
  let component: AdesaoDadosPessoaisTitularComponent;
  let fixture: ComponentFixture<AdesaoDadosPessoaisTitularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoDadosPessoaisTitularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoDadosPessoaisTitularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
