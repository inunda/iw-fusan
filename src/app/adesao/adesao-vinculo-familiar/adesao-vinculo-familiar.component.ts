import { Component, OnInit } from "@angular/core";
import { NextStepService } from "../../service/next-step.service";
import CPF, { validate } from "cpf-check";

@Component({
  selector: "app-adesao-vinculo-familiar",
  templateUrl: "./adesao-vinculo-familiar.component.html",
  styleUrls: ["./adesao-vinculo-familiar.component.css"]
})
export class AdesaoVinculoFamiliarComponent implements OnInit {
  stateValue: string = "";
  usercpf: string = "";
  check: boolean = false;

  nameError: boolean = false;
  sobrenomeError: boolean = false;

  userbirth: any = {
    tab: "three",
    nome: "",
    sobrenome: "",
    parentesco: "",
    cpf: "",
    check: false
  };

  public constructor(private NextStepService: NextStepService) {}
  public data: any = this.userbirth;

  onUserNomeChange(birthValue: string): void {
    if (!Number(birthValue)) {
      this.userbirth.nome = birthValue;
      this.NextStepService.myMethod(this.data);
      this.nameError = false;
    } else {
      this.nameError = true;
    }
  }

  onUserSobrenomeChange(rgValue: string): void {
    if (!Number(rgValue)) {
      this.userbirth.sobrenome = rgValue;
      this.NextStepService.myMethod(this.data);
      this.sobrenomeError = false;
    } else {
      this.sobrenomeError = true;
    }
  }

  onUserParentescoChange(sexValue: string): void {
    if (!Number(sexValue)) {
      this.userbirth.parentesco = sexValue;
      this.NextStepService.myMethod(this.data);
      this.sobrenomeError = false;
    } else {
      this.sobrenomeError = true;
    }
  }

  onUserCPFChange(stateValue: string): void {
    if (CPF.validate(stateValue)) {
      this.userbirth.cpf = stateValue;
      this.stateValue = stateValue;
      this.usercpf = stateValue;
      this.NextStepService.myMethod(this.data);
    } else {
      this.userbirth.cpf = "";
      this.stateValue = "";
      this.usercpf = "";
      this.NextStepService.myMethod(this.data);
    }
  }

  onUserCheckChange(natValue: boolean): void {
    this.userbirth.check = natValue;
    this.check = natValue;
    this.NextStepService.myMethod(this.data);
  }

  ngOnInit() {}
}
