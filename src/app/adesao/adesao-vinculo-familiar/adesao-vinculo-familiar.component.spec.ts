import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoVinculoFamiliarComponent } from './adesao-vinculo-familiar.component';

describe('AdesaoVinculoFamiliarComponent', () => {
  let component: AdesaoVinculoFamiliarComponent;
  let fixture: ComponentFixture<AdesaoVinculoFamiliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoVinculoFamiliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoVinculoFamiliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
