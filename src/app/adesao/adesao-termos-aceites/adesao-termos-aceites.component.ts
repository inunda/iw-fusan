import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NextStepService } from '../../service/next-step.service';

@Component({
  selector: 'app-adesao-termos-aceites',
  templateUrl: './adesao-termos-aceites.component.html',
  styleUrls: ['./adesao-termos-aceites.component.css']
})
export class AdesaoTermosAceitesComponent implements OnInit {
  newopt: any = false;
  userbirth: any = {
    tab: 'six',
    optone: false,
    opttwo: false,
    uspersonyes: '',
    uspersonno: '',
    politicoyes: '',
    politicono: '',
    residenciaoutropaisYes: '',
    residenciaoutropaisNo: '',
    nif: {
      on: false,
      number: '',
      country: '',
      arr: ''
    }
  };

  tiponedisabled: any = true;
  tiptwodisabled: any = true;

  usernifnumber: any = '';
  usernifcountry: any = 'default';

  btnlock: any = true;

  nifuser: any = {
    number: '',
    country: 'default'
  };

  nifarr: any = [];

  public constructor(private NextStepService: NextStepService) {}

  public data: any = this.userbirth;

  onChangeOptone(valormensal: number): void {
    this.userbirth.optone = valormensal;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptTwo(valoridade: number): void {
    this.userbirth.opttwo = valoridade;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptUsPersonYes(valoridade: any): void {
    this.userbirth.uspersonyes = valoridade;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptUsPersonNo(valoridade: any): void {
    this.userbirth.uspersonno = valoridade;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptPoliticoYes(valoridade: any): void {
    this.userbirth.politicoyes = valoridade;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptPoliticoNo(valoridade: any): void {
    this.userbirth.politicono = valoridade;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptMoraForaYes(valoridade: any): void {
    this.userbirth.residenciaoutropaisYes = valoridade;
    this.newopt = true;
    this.userbirth.nif.on = true;
    this.NextStepService.myMethod(this.data);
  }

  onChangeOptMoraForaNo(valoridade: any): void {
    this.userbirth.residenciaoutropaisNo = valoridade;
    this.newopt = false;
    this.userbirth.nif.on = false;
    this.NextStepService.myMethod(this.data);
  }

  onChangeNif(nif: any): void {
    this.userbirth.nif.nif = nif;
    this.usernifnumber = nif;
    if (
      this.userbirth.nif.country !== 'default' &&
      this.userbirth.nif.nif !== ''
    ) {
      this.btnlock = false;
    } else {
      this.btnlock = true;
      this.usernifnumber = '';
    }
  }

  onChangeCountry(country: any): void {
    this.userbirth.nif.country = country;
    this.usernifcountry = country;
    if (
      this.userbirth.nif.country !== 'default' &&
      this.userbirth.nif.nif !== ''
    ) {
      this.btnlock = false;
    } else {
      this.btnlock = true;
      this.usernifnumber = '';
    }
  }

  incluirNif() {
    console.log(this.nifarr);
    if (this.usernifcountry !== 'default' && this.usernifnumber !== '') {
      this.btnlock = false;
      this.nifuser.country = this.userbirth.nif.country;
      this.nifuser.number = this.userbirth.nif.nif;

      this.nifarr.push(this.nifuser);

      this.userbirth.nif.arr = this.nifarr;
      this.NextStepService.myMethod(this.data);

      this.nifuser = {
        number: '',
        country: 'default'
      };

      this.usernifnumber = '';
      this.usernifcountry = 'default';
    } else {
      this.btnlock = true;
    }
  }

  removeTodo(value) {
    const index = this.nifarr.indexOf(value);
    this.nifarr.splice(index, 1);
    if (this.nifarr.length === 0) {
      this.nifuser = {
        number: '',
        country: 'default'
      };

      this.usernifnumber = '';
      this.usernifcountry = 'default';
      this.userbirth.nif.arr = this.nifarr;
      this.NextStepService.myMethod(this.data);
    } else {
      this.userbirth.nif.arr = this.nifarr;
      this.NextStepService.myMethod(this.data);
    }
  }

  openOneModal() {
    this.tiponedisabled = false;
    this.tiptwodisabled = true;
  }

  openTwoModal() {
    this.tiptwodisabled = false;
    this.tiponedisabled = true;
  }

  closeOneModal() {
    this.tiponedisabled = true;
  }

  closeTwoModal() {
    this.tiptwodisabled = true;
  }

  ngOnInit() {}
}
