import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoTermosAceitesComponent } from './adesao-termos-aceites.component';

describe('AdesaoTermosAceitesComponent', () => {
  let component: AdesaoTermosAceitesComponent;
  let fixture: ComponentFixture<AdesaoTermosAceitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoTermosAceitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoTermosAceitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
