import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoBeneficiariosComponent } from './adesao-beneficiarios.component';

describe('AdesaoBeneficiariosComponent', () => {
  let component: AdesaoBeneficiariosComponent;
  let fixture: ComponentFixture<AdesaoBeneficiariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoBeneficiariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoBeneficiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
