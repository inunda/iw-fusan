import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import CPF, { validate } from "cpf-check";

import { NextStepService } from "../../service/next-step.service";

interface userinfo1 {
  tab: "eight";
  nome: "";
  cpf: "";
  date: "";
  celular: "";
  email: "";
  beneficios: "";
  parentesco: null;
  qnt: any;
}

@Component({
  selector: "app-adesao-beneficiarios",
  templateUrl: "./adesao-beneficiarios.component.html",
  styleUrls: ["./adesao-beneficiarios.component.css"]
})
export class AdesaoBeneficiariosComponent implements OnInit {
  todoArray = [];
  items = {};

  nome: any = "";
  cpf: any = "";
  date: any = "";
  celular: any = "";
  email: any = "";
  beneficios: any = "";
  parentesco: any = null;
  qnt: any = "";

  emailError: boolean = false;
  beneficiosError: boolean = false;
  nameError: boolean = false;

  validBeneficiarios: boolean = false;

  disbaledAdd: boolean = true;

  adicionarBeneficiarioDisabled: boolean = false;

  public constructor(
    private NextStepService: NextStepService,
    private modalService: NgbModal
  ) {}

  userinfo: userinfo1 = {
    tab: "eight",
    nome: "",
    cpf: "",
    date: "",
    celular: "",
    email: "",
    beneficios: "",
    parentesco: null,
    qnt: null
  };

  public data: any = this.userinfo;

  onChangeNome(nome: any): void {
    if (Number(nome)) {
      this.nameError = true;
      this.disbaledAdd = true;
    } else {
      this.userinfo.nome = nome;
      this.nome = nome;
      this.nameError = false;

      if (
        !this.beneficiosError &&
        this.nome !== "" &&
        this.cpf !== "" &&
        this.date !== "" &&
        (Number(this.beneficios) !== 0 && Number(this.beneficios) <= 100) &&
        this.parentesco !== null
      ) {
        this.disbaledAdd = false;
      } else {
        this.disbaledAdd = true;
      }
    }
  }

  onChangeCpf(cpf: any): void {
    if (CPF.validate(cpf)) {
      this.userinfo.cpf = cpf;
      this.cpf = cpf;
      if (
        !this.beneficiosError &&
        this.nome !== "" &&
        this.cpf !== "" &&
        this.date !== "" &&
        (Number(this.beneficios) !== 0 && Number(this.beneficios) <= 100) &&
        this.parentesco !== null
      ) {
        this.disbaledAdd = false;
      } else {
        this.disbaledAdd = true;
      }
    }
  }

  onChangeData(date: any): void {
    this.userinfo.date = date;
    this.date = date;
    if (
      !this.beneficiosError &&
      this.nome !== "" &&
      this.cpf !== "" &&
      this.date !== "" &&
      (Number(this.beneficios) !== 0 && Number(this.beneficios) <= 100) &&
      this.parentesco !== null
    ) {
      this.disbaledAdd = false;
    } else {
      this.disbaledAdd = true;
    }
  }

  onChangeCelular(celular: any): void {
    this.userinfo.celular = celular;
    this.celular = celular;
  }

  onChangeEmail(email: any): void {
    let re = /\S+@\S+\.\S+/;
    if (re.test(String(email).toLowerCase())) {
      this.userinfo.email = email;
      this.email = email;
      this.emailError = false;
      this.disbaledAdd = false;
    } else {
      this.emailError = true;
      this.disbaledAdd = true;
    }
  }
  onChangeBeneficios(beneficios: any): void {
    if (Number(beneficios) !== 0 && Number(beneficios) <= 100) {
      this.userinfo.beneficios = beneficios;
      this.beneficios = beneficios;
      this.beneficiosError = false;
      if (
        !this.beneficiosError &&
        this.nome !== "" &&
        this.cpf !== "" &&
        this.date !== "" &&
        (Number(this.beneficios) !== 0 && Number(this.beneficios) <= 100) &&
        this.parentesco !== null
      ) {
        this.disbaledAdd = false;
      } else {
        this.disbaledAdd = true;
      }
    } else {
      this.userinfo.beneficios = "";
      this.beneficios = "";
      this.beneficiosError = true;
      this.disbaledAdd = true;
    }
  }

  onChangeParentesco(parentesco: any): void {
    this.userinfo.parentesco = parentesco;
    this.parentesco = parentesco;
    this.userinfo.qnt = this.todoArray.length;
    if (
      !this.beneficiosError &&
      this.nome !== "" &&
      this.cpf !== "" &&
      this.date !== "" &&
      (Number(this.beneficios) !== 0 && Number(this.beneficios) <= 100) &&
      this.parentesco !== null
    ) {
      this.disbaledAdd = false;
    } else {
      this.disbaledAdd = true;
    }
  }

  addTodo() {
    let nums = [];
    this.disbaledAdd = false;
    this.items = {
      nome: this.nome,
      cpf: this.cpf,
      date: this.date,
      calular: this.celular,
      email: this.email,
      beneficios: this.beneficios,
      parentesco: this.parentesco
    };

    this.todoArray.push(this.items);
    this.userinfo.qnt = this.todoArray;
    this.NextStepService.myMethod(this.userinfo);

    this.nome = "";
    this.cpf = "";
    this.date = "";
    this.celular = "";
    this.email = "";
    this.beneficios = "";
    this.parentesco = null;
    this.qnt = "";

    for (let x = 0; x < this.todoArray.length; x++) {
      nums.push(Number(this.todoArray[x].beneficios));
    }

    if (nums.reduce((a, b) => a + b) == 100) {
      this.adicionarBeneficiarioDisabled = true;
    } else if (nums.reduce((a, b) => a + b) > 100) {
      this.adicionarBeneficiarioDisabled = true;
    } else {
      this.adicionarBeneficiarioDisabled = false;
    }
  }

  removeTodo(value) {
    console.log(this.todoArray);
    let index = this.todoArray.indexOf(value);
    this.todoArray.splice(index, 1);
    this.NextStepService.myMethod(this.data);
    this.adicionarBeneficiarioDisabled = false;
    if (this.todoArray.length == 0) {
      this.adicionarBeneficiarioDisabled = false;
    }
  }

  closeResult: string;

  open(content) {
    if (this.nome == "") {
      this.disbaledAdd = true;
      this.modalService
        .open(content, { ariaLabelledBy: "modal-basic-title" })
        .result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
    } else {
      this.disbaledAdd = false;
      this.modalService
        .open(content, { ariaLabelledBy: "modal-basic-title" })
        .result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit() {}
}
