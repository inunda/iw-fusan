import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NextStepService } from "../../service/next-step.service";
import { of } from "rxjs";

@Component({
  selector: "app-adesao-endereco",
  templateUrl: "./adesao-endereco.component.html",
  styleUrls: ["./adesao-endereco.component.css"]
})
export class AdesaoEnderecoComponent implements OnInit {
  resultado: any;
  result: any;
  endValue: string = "";
  bairroValue: string = "";
  localidadeValue: string = "";
  endNumValue: string = "";

  cepinvalid: boolean = false;

  userbirth: any = {
    tab: "two",
    cep: "",
    endereco: "",
    bairro: "",
    complemento: "",
    estado: "",
    cidade: "",
    endValue: ""
  };

  public constructor(
    private NextStepService: NextStepService,
    private http: HttpClient
  ) {}

  public data: any = this.userbirth;

  test: any;

  onUserCepChange(cepValue: string): void {
    this.userbirth.cep = cepValue;

    cepValue = cepValue.replace(/\D/g, "");
    if (cepValue !== "") {
      const validacep = /^[0-9]{8}$/;
      if (validacep.test(cepValue)) {
        this.http
          .get(`//viacep.com.br/ws/${cepValue}/json`)
          .subscribe(response => {
            console.group("CEP Validation");
            console.log(response);
            console.groupEnd();
            if (!(<any>response).erro) {
              this.test = response;
              this.onUserEndChange(this.test.logradouro);
              this.onUserBairroChange(this.test.bairro);
              this.onUserCidadeChange(this.test.localidade);
              this.endValue = this.test.logradouro;
              this.bairroValue = this.test.bairro;
              this.localidadeValue = this.test.localidade;
              this.cepinvalid = false;
            } else {
              this.onUserEndChange("");
              this.onUserBairroChange("");
              this.onUserCidadeChange("");
              this.endValue = "";
              this.bairroValue = "";
              this.localidadeValue = "";
              this.cepinvalid = true;
            }
          });
      }
    }
    this.NextStepService.myMethod(this.data);
  }

  onUserEndChange(endValue: string): void {
    this.userbirth.endereco = endValue;
    this.endValue = endValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserBairroChange(bairroValue: string): void {
    this.userbirth.bairro = bairroValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserCompChange(compValue: string): void {
    this.userbirth.complemento = compValue;
  }

  onUserEstadoChange(estadoValue: string): void {
    this.userbirth.estado = estadoValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserCidadeChange(cidadeValue: string): void {
    this.userbirth.cidade = cidadeValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserEndNumChange(numValue: string): void {
    this.userbirth.endValue = numValue;
    this.endNumValue = numValue;
    this.NextStepService.myMethod(this.data);
  }

  ngOnInit() {}
}
