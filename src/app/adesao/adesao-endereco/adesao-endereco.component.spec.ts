import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoEnderecoComponent } from './adesao-endereco.component';

describe('AdesaoEnderecoComponent', () => {
  let component: AdesaoEnderecoComponent;
  let fixture: ComponentFixture<AdesaoEnderecoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoEnderecoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoEnderecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
