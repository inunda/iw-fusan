import { Component, ViewEncapsulation, OnInit } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-adesao-dados-pessoais",
  templateUrl: "./adesao-dados-pessoais.component.html",
  styleUrls: ["./adesao-dados-pessoais.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class AdesaoDadosPessoaisComponent implements OnInit {
  userbirth: any = {
    tab: "one",
    birth: "",
    rgval: "",
    userx: "",
    users: "",
    usern: ""
  };

  menorDeIdade: boolean = false;

  public constructor(private NextStepService: NextStepService) {}
  public data: any = this.userbirth;

  onSearchChange(birthValue: string): void {
    if (new Date().getFullYear() - Number(birthValue.slice(0, 4)) > 18) {
      this.menorDeIdade = false;
      this.userbirth.birth = birthValue;
      this.NextStepService.myMethod(this.data);
    } else {
      this.menorDeIdade = true;
      this.userbirth.birth = birthValue;
      this.NextStepService.myMethod(this.data);
    }
  }

  onUserRgChange(rgValue: number): void {
    if (rgValue !== null && String(rgValue).length < 11 && rgValue !== NaN) {
      this.userbirth.rgval = rgValue;
      this.NextStepService.myMethod(this.data);
    }
  }

  onUserSexChange(sexValue: string): void {
    this.userbirth.userx = sexValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserStateChange(stateValue: string): void {
    this.userbirth.users = stateValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserNationalChange(natValue: string): void {
    this.userbirth.usern = natValue;
    this.NextStepService.myMethod(this.data);
  }

  ngOnInit() {}
}
