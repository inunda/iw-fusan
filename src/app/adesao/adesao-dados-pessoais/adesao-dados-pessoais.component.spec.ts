import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoDadosPessoaisComponent } from './adesao-dados-pessoais.component';

describe('AdesaoDadosPessoaisComponent', () => {
  let component: AdesaoDadosPessoaisComponent;
  let fixture: ComponentFixture<AdesaoDadosPessoaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoDadosPessoaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoDadosPessoaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
