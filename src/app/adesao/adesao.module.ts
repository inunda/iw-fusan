import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { routes } from "./adesao.routing";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { IMaskModule } from "angular-imask";

import { NextStepService } from "../service/next-step.service";

import { AdesaoComponent } from "./adesao/adesao.component";
import { AdesaoDadosPessoaisComponent } from "./adesao-dados-pessoais/adesao-dados-pessoais.component";
import { AdesaoDadosPessoaisTitularComponent } from "./adesao-dados-pessoais-titular/adesao-dados-pessoais-titular.component";
import { AdesaoEnderecoComponent } from "./adesao-endereco/adesao-endereco.component";
import { AdesaoVinculoFamiliarComponent } from "./adesao-vinculo-familiar/adesao-vinculo-familiar.component";
import { AdesaoTributacaoImpostoRendaComponent } from "./adesao-tributacao-imposto-renda/adesao-tributacao-imposto-renda.component";
import { AdesaoContribuicaoComponent } from "./adesao-contribuicao/adesao-contribuicao.component";
import { AdesaoBeneficiariosComponent } from "./adesao-beneficiarios/adesao-beneficiarios.component";
import { AdesaoTermosAceitesComponent } from "./adesao-termos-aceites/adesao-termos-aceites.component";
import { AdesaoAssinaturaEletronicaComponent } from "./adesao-assinatura-eletronica/adesao-assinatura-eletronica.component";
import { AdesaoVisualizarDadosComponent } from "./adesao-visualizar-dados/adesao-visualizar-dados.component";
import { AdesaoConfirmaDadosComponent } from './adesao-confirma-dados/adesao-confirma-dados.component'

@NgModule({
  declarations: [
    AdesaoComponent,
    AdesaoDadosPessoaisComponent,
    AdesaoDadosPessoaisTitularComponent,
    AdesaoEnderecoComponent,
    AdesaoVinculoFamiliarComponent,
    AdesaoTributacaoImpostoRendaComponent,
    AdesaoContribuicaoComponent,
    AdesaoBeneficiariosComponent,
    AdesaoTermosAceitesComponent,
    AdesaoAssinaturaEletronicaComponent,
    AdesaoVisualizarDadosComponent,
    AdesaoConfirmaDadosComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    IMaskModule
  ],
  exports: [FormsModule, IMaskModule],
  providers: [NextStepService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdesaoModule {}
