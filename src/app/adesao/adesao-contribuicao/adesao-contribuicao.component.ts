import { Component, OnInit, Input } from "@angular/core";

import { NextStepService } from "../../service/next-step.service";

@Component({
  selector: "app-adesao-contribuicao",
  templateUrl: "./adesao-contribuicao.component.html",
  styleUrls: ["./adesao-contribuicao.component.css"]
})
export class AdesaoContribuicaoComponent implements OnInit {
  rangeValueFmt: string = "";
  rangeValue: number = 50;
  idadeRange: any = 0;

  lessthan50: boolean = false;

  userbirth: any = {
    tab: "five",
    valueone: this.rangeValue,
    valuetwo: this.idadeRange,
    truevalue: this.lessthan50
  };

  public constructor(private NextStepService: NextStepService) {}

  public data: any = this.userbirth;

  onChangeInput(valormensal: any): void {
    const valor = Number(
      valormensal
        .replace(/R\$/g, "")
        .replace(/\./g, "")
        .replace(/\,00/g, "")
    );
    if (valor === null) {
      this.rangeValue = 50;
      this.NextStepService.myMethod(this.data);
    } else {
      if (valor >= 50 && valor <= 10000) {
        this.lessthan50 = false;
        this.userbirth.truevalue = this.lessthan50;
        const formatter = new Intl.NumberFormat("pt-BR", {
          style: "currency",
          currency: "BRL",
          minimumFractionDigits: 2
        });
        this.rangeValueFmt = formatter.format(valor);
        this.rangeValue = valor;
        this.userbirth.valueone = valor;

        this.NextStepService.myMethod(this.data);
      } else {
        this.lessthan50 = true;
        this.userbirth.truevalue = this.lessthan50;
        this.rangeValue = 50;
        this.NextStepService.myMethod(this.data);
      }
    }
  }

  onChangeInputValor(valoridade: any): void {
    if (valoridade === null || valoridade === "") {
      this.idadeRange = 0;
      this.NextStepService.myMethod(this.data);
    } else {
      if (Number(valoridade) <= 99) {
        this.idadeRange = valoridade;
        this.userbirth.valuetwo = Number(valoridade);
        this.NextStepService.myMethod(this.data);
      } else {
        this.idadeRange = 99;
        this.NextStepService.myMethod(this.data);
      }
    }
  }

  ngOnInit() {}
}
