import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoContribuicaoComponent } from './adesao-contribuicao.component';

describe('AdesaoContribuicaoComponent', () => {
  let component: AdesaoContribuicaoComponent;
  let fixture: ComponentFixture<AdesaoContribuicaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoContribuicaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoContribuicaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
