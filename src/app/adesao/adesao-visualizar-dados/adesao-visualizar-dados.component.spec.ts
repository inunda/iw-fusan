import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoVisualizarDadosComponent } from './adesao-visualizar-dados.component';

describe('AdesaoVisualizarDadosComponent', () => {
  let component: AdesaoVisualizarDadosComponent;
  let fixture: ComponentFixture<AdesaoVisualizarDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoVisualizarDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoVisualizarDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
