import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoTributacaoImpostoRendaComponent } from './adesao-tributacao-imposto-renda.component';

describe('AdesaoTributacaoImpostoRendaComponent', () => {
  let component: AdesaoTributacaoImpostoRendaComponent;
  let fixture: ComponentFixture<AdesaoTributacaoImpostoRendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoTributacaoImpostoRendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoTributacaoImpostoRendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
