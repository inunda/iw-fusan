import { Component, OnInit, QueryList } from "@angular/core";
import { NextStepService } from "../../service/next-step.service";

interface Calculo {
  base: string;
  aliquota: string;
}

const Base: Calculo[] = [
  {
    base: "Até R$1.999,18",
    aliquota: "ISENTO"
  },
  {
    base: "De R$1.999,18 até R$ 2.967,98",
    aliquota: "7,5%"
  },
  {
    base: "De R$ 2.967,98 até R$ 3.938,60",
    aliquota: "15%"
  },
  {
    base: "De R$ 3.938,60 até R$ 4.897,91",
    aliquota: "22,5%"
  },
  {
    base: "Acima de R$ 4.897,91",
    aliquota: "27,5%"
  }
];

interface Acumulacao {
  base: string;
  aliquota: string;
}
const Acumulacao: Acumulacao[] = [
  {
    base: "Até 2 anos",
    aliquota: "35%"
  },
  {
    base: "Acima de 2 anos até 4 anos",
    aliquota: "30%"
  },
  {
    base: "Acima de 4 anos até 6 anos",
    aliquota: "25%"
  },
  {
    base: "Acima de 6 anos até 8 anos",
    aliquota: "20%"
  },
  {
    base: "Acima de 6 anos até 8 anos",
    aliquota: "15%"
  },
  {
    base: "Acima de 10 anos",
    aliquota: "10%"
  }
];

@Component({
  selector: "app-adesao-tributacao-imposto-renda",
  templateUrl: "./adesao-tributacao-imposto-renda.component.html",
  styleUrls: ["./adesao-tributacao-imposto-renda.component.css"]
})
export class AdesaoTributacaoImpostoRendaComponent implements OnInit {
  userbirth: any = {
    tab: "four",
    one: false,
    two: false
  };
  base = Base;
  acum = Acumulacao;

  public constructor(private NextStepService: NextStepService) {}

  public data: any = this.userbirth;

  onUserOneChange(birthValue: string): void {
    this.userbirth.one = birthValue;
    this.NextStepService.myMethod(this.data);
  }

  onUserTwoChange(rgValue: string): void {
    this.userbirth.two = rgValue;
    this.NextStepService.myMethod(this.data);
  }

  ngOnInit() {}
}
