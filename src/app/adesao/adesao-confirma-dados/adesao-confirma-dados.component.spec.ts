import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdesaoConfirmaDadosComponent } from './adesao-confirma-dados.component';

describe('AdesaoConfirmaDadosComponent', () => {
  let component: AdesaoConfirmaDadosComponent;
  let fixture: ComponentFixture<AdesaoConfirmaDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdesaoConfirmaDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdesaoConfirmaDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
