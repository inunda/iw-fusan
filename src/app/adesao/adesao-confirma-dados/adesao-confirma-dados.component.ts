import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-adesao-confirma-dados",
  templateUrl: "./adesao-confirma-dados.component.html",
  styleUrls: ["./adesao-confirma-dados.component.css"]
})
export class AdesaoConfirmaDadosComponent implements OnInit {
  @Input() data;
  userinfo: any = {};

  constructor() {}

  ngOnInit() {
    this.userinfo = {
      tributacao: this.data.tributacao,
      valormensal: this.data.valormensal,
      qntbeneficiarios: this.data.qntbeneficiarios,
      beneficiarios: this.data.beneficiarios,
    };
  }
}
