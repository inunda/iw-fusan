import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SolicitacaoConcluidaComponent } from "./pages/solicitacao-concluida/solicitacao-concluida.component";
import { AdesaoConcluidaComponent } from "./pages/adesao-concluida/adesao-concluida.component";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then(m => m.HomeModule)
  },

  {
    path: "usercpf",
    loadChildren: () =>
      import("./adesao-cpf/adesao-cpf.module").then(m => m.AdesaoCpfModule)
  },

  {
    path: "usercpf",
    loadChildren: () =>
      import("./adesao-cpf/adesao-cpf.module").then(m => m.AdesaoCpfModule)
  },

  {
    path: "preadesao",
    loadChildren: () =>
      import("./solicitacao-adesao/solicitacao-adesao.module").then(
        m => m.SolicitacaoAdesaoModule
      )
  },

  {
    path: "adesao",
    loadChildren: () =>
      import("./adesao/adesao.module").then(m => m.AdesaoModule)
  },
  {
    path: "solicitacao-adesao-concluido",
    component: SolicitacaoConcluidaComponent
  },
  { path: "adesao-concluido", component: AdesaoConcluidaComponent },
  { path: "**", redirectTo: "home" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
